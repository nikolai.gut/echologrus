package echologrus

import (
	"github.com/labstack/gommon/log"
	"github.com/mailgun/logrus-hooks/udploghook"
	"github.com/sirupsen/logrus"
	"io"
)

type Logrus struct {
	*logrus.Logger
}

// Logger ...
var Logger *logrus.Logger

func GetEchoLogger(host string, port int) (Logrus, error) {
	Logger = logrus.New()
	Logger.SetFormatter(&logrus.JSONFormatter{})
	hook, err := udploghook.New(host, port)
	if err != nil {
		return Logrus{Logger}, err
	}
	Logger.AddHook(hook)
	return Logrus{Logger}, nil
}

// Level returns testLogger level
func (l Logrus) Level() log.Lvl {
	switch l.Logger.Level {
	case logrus.DebugLevel:
		return log.DEBUG
	case logrus.WarnLevel:
		return log.WARN
	case logrus.ErrorLevel:
		return log.ERROR
	case logrus.InfoLevel:
		return log.INFO
	default:
		l.Warn("Invalid level")
	}

	return log.OFF
}

// SetHeader is a stub to satisfy interface
// It's controlled by Logger
func (l Logrus) SetHeader(_ string) {}

// SetPrefix It's controlled by Logger
func (l Logrus) SetPrefix(s string) {}

// Prefix It's controlled by Logger
func (l Logrus) Prefix() string {
	return ""
}

// SetLevel set level to testLogger from given log.Lvl
func (l Logrus) SetLevel(lvl log.Lvl) {
	switch lvl {
	case log.DEBUG:
		Logger.SetLevel(logrus.DebugLevel)
	case log.WARN:
		Logger.SetLevel(logrus.WarnLevel)
	case log.ERROR:
		Logger.SetLevel(logrus.ErrorLevel)
	case log.INFO:
		Logger.SetLevel(logrus.InfoLevel)
	default:
		Logger.SetLevel(99)
	}
}

// Output testLogger output func
func (l Logrus) Output() io.Writer {
	return l.Out
}

// SetOutput change output, default os.Stdout
func (l Logrus) SetOutput(w io.Writer) {
	Logger.SetOutput(w)
}

// Printj print json log
func (l Logrus) Printj(j log.JSON) {
	Logger.WithFields(logrus.Fields(j)).Print()
}

// Debugj debug json log
func (l Logrus) Debugj(j log.JSON) {
	Logger.WithFields(logrus.Fields(j)).Debug()
}

// Infoj info json log
func (l Logrus) Infoj(j log.JSON) {
	Logger.WithFields(logrus.Fields(j)).Info()
}

// Warnj warning json log
func (l Logrus) Warnj(j log.JSON) {
	Logger.WithFields(logrus.Fields(j)).Warn()
}

// Errorj error json log
func (l Logrus) Errorj(j log.JSON) {
	Logger.WithFields(logrus.Fields(j)).Error()
}

// Fatalj fatal json log
func (l Logrus) Fatalj(j log.JSON) {
	Logger.WithFields(logrus.Fields(j)).Fatal()
}

// Panicj panic json log
func (l Logrus) Panicj(j log.JSON) {
	Logger.WithFields(logrus.Fields(j)).Panic()
}

// Print string log
func (l Logrus) Print(i ...interface{}) {
	Logger.Print(i...)
}

// Debug string log
func (l Logrus) Debug(i ...interface{}) {
	Logger.Debug(i...)
}

// Info string log
func (l Logrus) Info(i ...interface{}) {
	Logger.Info(i...)
}

// Warn string log
func (l Logrus) Warn(i ...interface{}) {
	Logger.Warn(i...)
}

// Error string log
func (l Logrus) Error(i ...interface{}) {
	Logger.Error(i...)
}

// Fatal string log
func (l Logrus) Fatal(i ...interface{}) {
	Logger.Fatal(i...)
}

// Panic string log
func (l Logrus) Panic(i ...interface{}) {
	Logger.Panic(i...)
}
